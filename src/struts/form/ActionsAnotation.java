package struts.form;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface ActionsAnotation {
	
	String path();
	String beanName();
	String actionClass(); 
	String formClasss();
	
    ActionAnotation[] mapurl();
}
