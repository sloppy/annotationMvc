package struts.form;

import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;




public class ActionListener implements ServletContextListener {

	@Override 
	public void contextDestroyed(ServletContextEvent arg0) {

		System.out.println("信息：系统已注销");
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		
		ServletContext context = arg0.getServletContext();
		String actionPath = context.getInitParameter("parseActionPathMap");
		try{
			Map<String,AnotationBean> map = AnotationParser.strutsAnotation();
			context.setAttribute(actionPath, map);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("信息：系统已加载完成");
		
	}

	
}
