package struts.action;

import java.io.IOException;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import struts.form.ActionForm;
import struts.form.AnotationBean;
import struts.form.FullForm;


public class ActionServlet extends HttpServlet {

	/**
	 * 
	 */ 
	private static final long serialVersionUID = -3137831652369054389L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//String name = request.getParameter("text");
		// //获得请求头部
		String path = this.getPath(request.getServletPath());
		Map<String,AnotationBean> map = (Map<String, AnotationBean>) this.getServletContext().getAttribute("actionPath");
		AnotationBean anotation = map.get(path);
		String formClass = anotation.getFormClass();
		ActionForm form = FullForm.full(formClass, request);
		String actionType = anotation.getActionClass();
		Action action = null;
		String url = "";
		try{
			Class clazz = Class.forName(actionType);
			action = (Action) clazz.newInstance();
			url = action.execute(request, form, anotation.getActionForward());
		}catch(Exception e){
			System.out.println("控制器异常："+e);
		}
		
		RequestDispatcher dis = request.getRequestDispatcher(url);
		dis.forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


		  this.doGet(request, response);
	}
	
	private String getPath(String servlertPath){
		
		return servlertPath.split("\\.")[0];
		
	}
	

}
